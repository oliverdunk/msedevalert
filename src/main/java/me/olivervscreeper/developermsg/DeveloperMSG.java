package me.olivervscreeper.developermsg;

import net.pushover.client.PushoverClient;
import net.pushover.client.PushoverException;
import net.pushover.client.PushoverMessage;
import net.pushover.client.PushoverRestClient;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class DeveloperMSG extends JavaPlugin {

    PushoverClient client;

    public void onEnable(){
        this.getLogger().info("ENABLED!");
        client = new PushoverRestClient();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!label.equals("scream")) return true;
        if(!sender.hasPermission("scream"))
        if(args.length == 0) return true;
        String message = "From " + sender.getName() + ":";
            for(int i = 0; i < args.length; i++){
                message = message + " " + args[i];
            }
            sendPushoverMessage(message);
        return true;
    }

    private void sendPushoverMessage(String message) {
        try {
            client.pushMessage(PushoverMessage.builderWithApiToken("aAaSsvDXrgVT74dEDKXTGNPc1UV5Qb").setUserId("fnFFqAM8CmqcCyGgYu21v59z7WsJvB").setMessage(message).setTitle("DevMSG").build());
            client.pushMessage(PushoverMessage.builderWithApiToken("aAaSsvDXrgVT74dEDKXTGNPc1UV5Qb").setUserId("ufSePvppmR1DBAKpkAXPQ4yBCDUQKZ").setMessage(message).setTitle("DevMSG").build());
            client.pushMessage(PushoverMessage.builderWithApiToken("aAaSsvDXrgVT74dEDKXTGNPc1UV5Qb").setUserId("ujFeJyjYFJd2Lwbygfw9cSCoeYiLBi").setMessage(message).setTitle("DevMSG").build());
        }catch(PushoverException e){
            return;
        }
    }
}
